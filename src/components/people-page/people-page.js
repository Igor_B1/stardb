import React from 'react';
import ItemList from "../item-list/item-list";
import PersonDetails from "../person-details/person-details";
import ErrorIndicator from "../error-indicator/error-indicator";
import SwapiService from "../../services/SwapiService";


const Row = ({left, right}) => (
    <div className="row mb-2">
        <div className="col-md-6">
            {left}
        </div>
        <div className="col-md-6">
            {right}
        </div>
    </div>
);




export default class PeoplePage extends React.Component {
    swapiService = new SwapiService();

    state = {
        selectedPerson: null,
        hasError: false
    };


    componentDidCatch(error, info) {
        this.setState({
            hasError: true
        });
    }

    onPersonSelected = (selectedPerson) => {
        this.setState({ selectedPerson });
    };

    render() {

        if (this.state.hasError) {
            return <ErrorIndicator/>;
        }

        const itemList = (
            <ItemList
                onItemSelected={this.onPersonSelected}
                getData={this.swapiService.getAllPeople}
                renderItem={(item) => `${item.name} ${item.gender} ${item.birthYear}`}
            />
        );

        const personDetails = (
            <PersonDetails personId={this.state.selectedPerson}/>
        );

        return <Row left={itemList} right={personDetails}/>
    }
}